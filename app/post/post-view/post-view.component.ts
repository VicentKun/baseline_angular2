import { Component } from '@angular/core';

@Component({
	moduleId: module.id,
	selector: 'my-post',
	template: '<h3>Vista de un post</h3>'
})

export class PostViewComponent {

	title = 'Postureo Literario Vista';

}