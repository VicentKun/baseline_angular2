import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PostListComponent } from '../post/post-list/post-list.component'
import { PostViewComponent } from '../post/post-view/post-view.component'


const routes: Routes = [
	//{ path: '', redirectTo: '/inicio', pathMatch: 'full' },
	{ path: '', component: PostListComponent },
	{ path: 'inicio', component: PostListComponent },
	{ path: 'ver-post', component: PostViewComponent }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})

export class AppRoutingModule {

	// Export this class.

}