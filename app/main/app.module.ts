import { NgModule, /*enableProdMode*/ }      from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';

import { PostListComponent } from '../post/post-list/post-list.component';
import { PostViewComponent } from '../post/post-view/post-view.component';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

/*enableProdMode();*/ // Descomentar para el modo producción

@NgModule({
  imports:      [ BrowserModule, AppRoutingModule ],
  declarations: [ AppComponent, PostListComponent, PostViewComponent ],
  providers: [ Title ],
  bootstrap: [ AppComponent ]
})

export class AppModule { 

	

}